create or replace FUNCTION CALCULATE_DIFF_BETWEEN_DATES 
( START_DATE IN DATE, END_DATE IN DATE ) 
RETURN NUMBER
IS 
    DIFF NUMBER;
BEGIN
    DIFF := END_DATE - START_DATE;
    RETURN DIFF;
END;
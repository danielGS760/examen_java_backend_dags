package com.prueba.java.examen.dao;

import java.time.LocalDate;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.prueba.java.examen.model.Employee;

@Repository
public interface IEmployeeDao extends JpaRepository<Employee, Integer> {

	@Procedure(name = "Employee.createEmployee")
	public Map<?, ?> createEmployee(@Param("GENDER_ID") Integer genderId, @Param("JOB_ID") Integer jobId,
			@Param("EMPLOYEE_NAME") String name, @Param("EMPLOYEE_LAST_NAME") String lastName,
			@Param("BIRTHDATE") LocalDate date);

	@Procedure(name = "Employee.workedHours")
	public Map<?, ?> getEmployeeWorkedHours(@Param("EMPLOYEE_ID") Integer employeeId,
			@Param("START_DATE") LocalDate startDate, @Param("END_DATE") LocalDate endDate);
	
	@Procedure(name = "Employee.paymentHours")
	public Map<?, ?> getEmployeePaymentHours(@Param("EMP_ID") Integer employeeId,
			@Param("START_DATE") LocalDate startDate, @Param("END_DATE") LocalDate endDate);

}

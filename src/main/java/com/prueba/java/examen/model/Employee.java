package com.prueba.java.examen.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.OneToOne;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "EMPLOYEES")
@NamedStoredProcedureQuery(name = "Employee.createEmployee", procedureName = "ADD_EMPLOYEE", parameters = {
		@StoredProcedureParameter(mode = ParameterMode.IN, name = "GENDER_ID", type = Integer.class),
		@StoredProcedureParameter(mode = ParameterMode.IN, name = "JOB_ID", type = Integer.class),
		@StoredProcedureParameter(mode = ParameterMode.IN, name = "EMPLOYEE_NAME", type = String.class),
		@StoredProcedureParameter(mode = ParameterMode.IN, name = "EMPLOYEE_LAST_NAME", type = String.class),
		@StoredProcedureParameter(mode = ParameterMode.IN, name = "BIRTHDATE", type = LocalDate.class),
		@StoredProcedureParameter(mode = ParameterMode.OUT, name = "EMPLOYEE_ID", type = Integer.class),
		@StoredProcedureParameter(mode = ParameterMode.OUT, name = "MESSAGE", type = String.class) })
@NamedStoredProcedureQuery(name = "Employee.workedHours", procedureName = "GET_EMPLOYEE_WORKED_HOURS", parameters = {
		@StoredProcedureParameter(mode = ParameterMode.IN, name = "EMPLOYEE_ID", type = Integer.class),
		@StoredProcedureParameter(mode = ParameterMode.IN, name = "START_DATE", type = LocalDate.class),
		@StoredProcedureParameter(mode = ParameterMode.IN, name = "END_DATE", type = LocalDate.class),
		@StoredProcedureParameter(mode = ParameterMode.OUT, name = "WORKED_HOURS", type = Integer.class),
		@StoredProcedureParameter(mode = ParameterMode.OUT, name = "MESSAGE", type = String.class) })
@NamedStoredProcedureQuery(name = "Employee.paymentHours", procedureName = "GET_EMPLOYEE_PAYMENT_HOURS", parameters = {
		@StoredProcedureParameter(mode = ParameterMode.IN, name = "EMP_ID", type = Integer.class),
		@StoredProcedureParameter(mode = ParameterMode.IN, name = "START_DATE", type = LocalDate.class),
		@StoredProcedureParameter(mode = ParameterMode.IN, name = "END_DATE", type = LocalDate.class),
		@StoredProcedureParameter(mode = ParameterMode.OUT, name = "PAYMENT_HOURS", type = Double.class),
		@StoredProcedureParameter(mode = ParameterMode.OUT, name = "MESSAGE", type = String.class) })
public class Employee {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@Column(name = "name")
	private String name;

	@Column(name = "last_name")
	@JsonProperty("last_name")
	private String lastName;

	@Column(name = "birthdate")
	private LocalDate birthdate; // dd-MM-YYYY

	@OneToOne
	@JoinColumn(referencedColumnName = "id")
	private Job job;

	@OneToOne
	@JoinColumn(referencedColumnName = "id")
	private Gender gender;

	public Employee() {
		super();
	}

	public Employee(String name, String lastName, LocalDate birthdate, Job job, Gender gender) {
		super();
		this.name = name;
		this.lastName = lastName;
		this.birthdate = birthdate;
		this.job = job;
		this.gender = gender;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public LocalDate getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(LocalDate birthdate) {
		this.birthdate = birthdate;
	}

	public Job getJob() {
		return job;
	}

	public void setJob(Job job) {
		this.job = job;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

}

package com.prueba.java.examen.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.prueba.java.examen.dao.IEmployeeDao;
import com.prueba.java.examen.exception.InputDataException;
import com.prueba.java.examen.model.Employee;
import com.prueba.java.examen.model.Job;

@Service
public class EmployeesService {

	@Autowired
	private IEmployeeDao dao;

	public int createEmployee(Employee employee) throws Exception {

		Map<?, ?> daoResult = dao.createEmployee(employee.getGender().getId(), employee.getJob().getId(),
				employee.getName(), employee.getLastName(), employee.getBirthdate());

		System.out.println(daoResult.keySet());

		int employeeId = (int) daoResult.get("EMPLOYEE_ID");

		if (employeeId <= 0) {
			throw new InputDataException((String) daoResult.get("MESSAGE"));
		}

		return employeeId;
	}

	public List<Employee> getEmployeesByJobId(Integer jobId) {

		// Consultar listado de empleados
		List<Employee> employees = dao.findAll(Sort.by(Sort.Direction.ASC, "id"));
		List<Employee> employeesFilteredByJobId = new ArrayList<>();

		if (employees != null) {
			// Filtrar los empleados por puesto indicado
			employeesFilteredByJobId = employees.stream().filter(employee -> {
				Job job = employee.getJob();
				return job.getId() == jobId;
			}).collect(Collectors.toList());
		}

		// Ordenamos por Last Name
		employeesFilteredByJobId.sort(
				(Employee employee1, Employee employee2) -> employee1.getLastName().compareTo(employee2.getLastName()));

		// Agrupamos por last name
		Map<String, List<Employee>> employeesPerLastName = employeesFilteredByJobId.stream()
				.collect(Collectors.groupingBy(Employee::getLastName));

		System.out.println("@@@ Employees grouped by last_name: " + new Gson().toJson(employeesPerLastName));

		return employeesFilteredByJobId.size() > 0 ? employeesFilteredByJobId : null;
	}

	public int getEmployeeWorkedHours(Integer employeeId, LocalDate startDate, LocalDate endDate) throws Exception {

		Map<?, ?> daoResult = dao.getEmployeeWorkedHours(employeeId, startDate, endDate);

		System.out.println(daoResult.keySet());

		int workedHours = daoResult.get("WORKED_HOURS") != null ? (int) daoResult.get("WORKED_HOURS") : 0;

		if (workedHours < 0) {
			throw new InputDataException((String) daoResult.get("MESSAGE"));
		}

		return workedHours;
	}

	public double getEmployeePaymentHours(Integer employeeId, LocalDate startDate, LocalDate endDate) throws Exception {

		Map<?, ?> daoResult = dao.getEmployeePaymentHours(employeeId, startDate, endDate);

		System.out.println(daoResult.keySet());

		double paymentHours = daoResult.get("PAYMENT_HOURS") != null ? (double) daoResult.get("PAYMENT_HOURS") : 0;

		if (paymentHours < 0) {
			throw new InputDataException((String) daoResult.get("MESSAGE"));
		}

		return paymentHours;
	}

	public List<Employee> getEmployees(List<Integer> employeesId) {
		List<Employee> employees = new ArrayList<Employee>();
		employeesId.forEach(id -> {
			EmployeeThread thread = new EmployeeThread(String.valueOf(id), id, dao);
			thread.run();
			if (thread.getEmployee() != null) {
				employees.add(thread.getEmployee());
			}
		});
		return employees;
	}

}

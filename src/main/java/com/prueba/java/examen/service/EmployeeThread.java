package com.prueba.java.examen.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import com.prueba.java.examen.dao.IEmployeeDao;
import com.prueba.java.examen.model.Employee;

public class EmployeeThread extends Thread {

	@Autowired
	private IEmployeeDao dao;

	private Integer employeeId;

	private Employee employee;

	public EmployeeThread(String name, Integer employeeId, IEmployeeDao dao) {
		super(name);
		this.dao = dao;
		this.employeeId = employeeId;
	}

	public Integer getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}

	public Employee getEmployee() {
		return employee;
	}

	@Override
	public void run() {

		Optional<Employee> result = dao.findById(employeeId);

		if (result.isPresent()) {
			employee = result.get();
		} else {
			System.out.println(String.format("@@@ Empleado %s no existe", employeeId));
		}
	}

}

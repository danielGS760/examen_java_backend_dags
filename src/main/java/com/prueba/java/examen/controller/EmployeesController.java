package com.prueba.java.examen.controller;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import com.prueba.java.examen.api.EmployeesApi;
import com.prueba.java.examen.dto.Input;
import com.prueba.java.examen.dto.Output;
import com.prueba.java.examen.exception.InputDataException;
import com.prueba.java.examen.model.Employee;
import com.prueba.java.examen.model.Gender;
import com.prueba.java.examen.model.Job;
import com.prueba.java.examen.service.EmployeesService;

@RestController
public class EmployeesController implements EmployeesApi {

	@Autowired
	private EmployeesService service;

	@Override
	public ResponseEntity<Output> createEmployee(Input employee) {

		Output response = new Output();

		Job job = new Job();
		Gender gender = new Gender();

		job.setId(employee.getJobId());
		gender.setId(employee.getGenderId());

		try {
			response.setSuccess(true);
			response.setId(service.createEmployee(
					new Employee(employee.getName(), employee.getLastName(), employee.getBirthdate(), job, gender)));
		} catch (InputDataException exception) {
			response.setSuccess(false);
			response.setMessage(exception.getMessage());
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		} catch (Exception e) {
			response.setSuccess(false);
			response.setMessage(e.getMessage());
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
		}

		return ResponseEntity.status(HttpStatus.CREATED).body(response);

	}

	@Override
	public ResponseEntity<Output> getEmployees(Integer jobId) {

		Output response = new Output();
		List<Employee> result = service.getEmployeesByJobId(jobId);

		response.setSuccess(result != null && result.size() > 0);
		response.setEmployees(response.isSuccess() ? result : null);

		return ResponseEntity.status(response.isSuccess() ? HttpStatus.OK : HttpStatus.NOT_FOUND).body(response);
	}

	@Override
	public ResponseEntity<Output> getEmployeeWorkedHours(Integer employeeId, String startDate, String endDate) {

		Output response = new Output();

		try {
			response.setSuccess(true);
			response.setTotalWorkedHours(
					service.getEmployeeWorkedHours(employeeId, LocalDate.parse(startDate), LocalDate.parse(endDate)));
		} catch (InputDataException exception) {
			response.setSuccess(false);
			response.setMessage(exception.getMessage());
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		} catch (Exception e) {
			response.setSuccess(false);
			response.setMessage(e.getMessage());
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
		}

		return ResponseEntity.status(HttpStatus.OK).body(response);
	}

	@Override
	public ResponseEntity<Output> getEmployeeWorkedHoursPayment(Integer employeeId, String startDate, String endDate) {

		Output response = new Output();

		try {
			response.setSuccess(true);
			response.setPayment(
					service.getEmployeePaymentHours(employeeId, LocalDate.parse(startDate), LocalDate.parse(endDate)));
		} catch (InputDataException exception) {
			response.setSuccess(false);
			response.setMessage(exception.getMessage());
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
		} catch (Exception e) {
			response.setSuccess(false);
			response.setMessage(e.getMessage());
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
		}

		return ResponseEntity.status(HttpStatus.OK).body(response);
	}

	@Override
	public ResponseEntity<Output> getEmployees(Integer[] employeesId) {
		Output response = new Output();
		List<Employee> result = service.getEmployees(Arrays.asList(employeesId));

		response.setSuccess(result != null && result.size() > 0);
		response.setEmployees(response.isSuccess() ? result : null);

		return ResponseEntity.status(response.isSuccess() ? HttpStatus.OK : HttpStatus.NOT_FOUND).body(response);
	}

}

package com.prueba.java.examen.exception;

public class InputDataException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8544628004109297024L;
	
	public InputDataException(String message) {
		super(message);
	}

}

package com.prueba.java.examen.api;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.prueba.java.examen.dto.Input;
import com.prueba.java.examen.dto.Output;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(path = "/employees")
public interface EmployeesApi {

	// Ejercicio 1
	@PostMapping(path = "/", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Output> createEmployee(@RequestBody(required = true) Input employee);

	// Ejercicio 2
	@GetMapping(path = "/", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Output> getEmployees(@RequestParam(required = true, name = "job_id") Integer jobId);
	
	//Ejercicio 3
	@GetMapping(path = "/thread", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Output> getEmployees(@RequestParam(required = true, name = "employee_id") Integer[] employeesId);

	// Ejercicio 4
	@GetMapping(path = "/{employee_id}/worked-hours", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Output> getEmployeeWorkedHours(@PathVariable(required = true, name = "employee_id") Integer employeeId,
			@RequestParam(required = true, name = "start_date") String startDate,
			@RequestParam(required = true, name = "end_date") String endDate);

	// Ejercicio 5
	@GetMapping(path = "/{employee_id}/payment", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Output> getEmployeeWorkedHoursPayment(@PathVariable(required = true, name = "employee_id") Integer employeeId,
			@RequestParam(required = true, name = "start_date") String startDate,
			@RequestParam(required = true, name = "end_date") String endDate);

}

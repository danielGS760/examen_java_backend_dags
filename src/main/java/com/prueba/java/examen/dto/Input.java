package com.prueba.java.examen.dto;

import java.io.Serializable;
import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class Input implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1288796447567759138L;

	@JsonProperty("gender_id")
	private Integer genderId;// Id del catálogo genders

	@JsonProperty("job_id")
	private Integer jobId;// Id del catálogo jobs

	@JsonProperty("last_name")
	private String lastName; // Apellido del empleado

	private String name; // Nombre del empleado
	private LocalDate birthdate; // "1983-01-01"

	public Integer getGenderId() {
		return genderId;
	}

	public void setGenderId(Integer genderId) {
		this.genderId = genderId;
	}

	public Integer getJobId() {
		return jobId;
	}

	public void setJobId(Integer jobId) {
		this.jobId = jobId;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDate getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(LocalDate birthdate) {
		this.birthdate = birthdate;
	}

}

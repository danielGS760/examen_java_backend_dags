package com.prueba.java.examen.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.prueba.java.examen.model.Employee;

@JsonInclude(Include.NON_NULL)
public class Output implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3131957246151657981L;

	@JsonProperty("total_worked_hours")
	private Integer totalWorkedHours;
	private boolean success;
	private String message;
	private Double payment;
	private Integer id;

	private List<Employee> employees;

	public Integer getTotalWorkedHours() {
		return totalWorkedHours;
	}

	public void setTotalWorkedHours(Integer totalWorkedHours) {
		this.totalWorkedHours = totalWorkedHours;
	}

	public Double getPayment() {
		return payment;
	}

	public void setPayment(Double payment) {
		this.payment = payment;
	}

	public List<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}

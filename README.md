# examen_java_backend_DAGS

Proyecto que incluye la resolución al examen practico.

## Herramientas utilizadas

- Base de datos Oracle express edition 21c.
- Java 11
- Spring boot 2.7.5
- Oracle SQL Developer version 22.2.1.234
- Eclipse IDE version: 2021-03 (4.19.0)

## Proyecto

- El proyecto ha sido generado a través de https://start.spring.io/
- Para la interacción con la capa de persistencia se ha utilizado JPA.
- En el directorio src/man/resources/postman se incluye la collección json para ser utilizada en postman.
- En el directorio src/man/resources/sql se incluyen los scripts utilizados para bases de datos, los cuales son:
	
		- Funciones:
			- CALCULATE_DIFF_BETWEEN_DATES.sql
			- CALCULATE_WORKED_HOURS.sql
			- VALIDATE_EMPLOYEE.sql
		- Stored procedures::
			- ADD_EMPLOYEE_SP.sql
			- GET_EMPLOYEE_PAYMENT_HOURS.sql
			- GET_EMPLOYEE_WORKED_HOURS.sql
		- Secuencias:
			- sequences.sql
		- tablas:	
			- tables.sql

## Enpoints

![create_employee](/images/POST-Create.png)
![create_employee](/images/GET-Employees.png)
![create_employee](/images/GET-Data.png)
